import 'package:json_annotation/json_annotation.dart';

part 'beer_dto.g.dart';

@JsonSerializable()
class BeerDto {
  final String? name;
  final String? tagline;
  final String? first_brewed;
  final String? description;
  final String? image_url;
  final double? abv;
  final int? ibu;
  final int? target_fg;
  final int? ebc;
  final double? ph;

  factory BeerDto.fromJson(Map<String, dynamic> json) => _$BeerDtoFromJson(json);

  BeerDto({this.name, this.tagline, this.first_brewed, this.description, this.image_url, this.abv, this.ibu, this.target_fg, this.ebc, this.ph});
}