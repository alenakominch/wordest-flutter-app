// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beer_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BeerDto _$BeerDtoFromJson(Map<String, dynamic> json) {
  return BeerDto(
    name: json['name'] as String?,
    tagline: json['tagline'] as String?,
    first_brewed: json['first_brewed'] as String?,
    description: json['description'] as String?,
    image_url: json['image_url'] as String?,
    abv: (json['abv'] as num?)?.toDouble(),
    ibu: json['ibu'] as int?,
    target_fg: json['target_fg'] as int?,
    ebc: json['ebc'] as int?,
    ph: (json['ph'] as num?)?.toDouble(),
  );
}

Map<String, dynamic> _$BeerDtoToJson(BeerDto instance) => <String, dynamic>{
      'name': instance.name,
      'tagline': instance.tagline,
      'first_brewed': instance.first_brewed,
      'description': instance.description,
      'image_url': instance.image_url,
      'abv': instance.abv,
      'ibu': instance.ibu,
      'target_fg': instance.target_fg,
      'ebc': instance.ebc,
      'ph': instance.ph,
    };
