
class GetBeersBody {
  final double? abvGt; // Returns all beers with ABV greater than the supplied number
  final double? abvLt; // Returns all beers with ABV less than the supplied number

  GetBeersBody({
    this.abvGt,
    this.abvLt,
  });

  Map<String, dynamic> toApi() {
    return {
      'abv_gt': abvGt,
      'abv_lt': abvLt,
    };
  }
}