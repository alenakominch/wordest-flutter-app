import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import 'package:wordest/data/remote/model/beer_dto.dart';

part 'beer_service.g.dart';

@RestApi(baseUrl: 'https://api.punkapi.com/v2/beers/')
abstract class BeerApi {
  factory BeerApi(Dio dio, {String baseUrl}) = _BeerApi;

  @GET('/random')
  Future<List<BeerDto>> getRandomBeer();
}

