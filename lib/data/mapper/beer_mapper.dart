import 'package:intl/intl.dart';
import 'package:wordest/data/remote/model/beer_dto.dart';
import 'package:wordest/domain/model/beer.dart';

extension BeerMapper on BeerDto {
  Beer toModel() => Beer(
    name: name ?? 'Noname Beer',
    tagline: tagline ?? 'Unknown tagline',
    firstBrewed: DateFormat.yMMM().format(DateFormat("MM/yyyy").parse(first_brewed ?? "01/1970")),
    description: description ?? '',
    imageUrl: image_url,
    abv: '${abv ?? '?'}%',
    ibu: '$ibu IBU',
    targetFg: '${target_fg ?? '?'}%',
    ebc: ebc,
    ph: ph,
  );
}
