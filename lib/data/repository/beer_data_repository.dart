
import 'package:wordest/data/remote/service/beer_service.dart';
import 'package:wordest/data/mapper/beer_mapper.dart';
import 'package:wordest/domain/model/beer.dart';
import 'package:wordest/domain/repository/beer_repository.dart';

class BeerDataRepository extends BeerRepository {
  final BeerApi _beerApi;

  BeerDataRepository(this._beerApi);

  @override
  Future<Beer> getRandomBeer() async {
    final response = await _beerApi.getRandomBeer();
    return response[0].toModel();
  }
}
