import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:wordest/domain/state/home/home_state.dart';
import 'package:wordest/internal/dependencies/home_module.dart';

class MyHomePage extends StatelessWidget {

  final HomeState _homeState = HomeModule.homeState();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: FocusScope.of(context).unfocus,
      child: Scaffold(
        body: _getBody(),
      ),
    );
  }

  Widget _getBody() {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 20),
            ElevatedButton(
              child: Text('Получить'),
              onPressed: _getRandomBeer,
            ),
            SizedBox(height: 20),
            _getBeerInfo(),
          ],
        ),
      ),
    );
  }

  final String noname = "Noname";

  Widget _getBeerInfo() {
    return Observer(
      builder: (_) {
        if (_homeState.isLoading)
          return Center(
            child: CircularProgressIndicator(),
          );
        if (_homeState.beer == null) return Container();
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('Name: ${_homeState.beer!.name}'),
          ],
        );
      },
    );
  }

  void _getRandomBeer() {
    _homeState.getRandomBeer();
  }
}