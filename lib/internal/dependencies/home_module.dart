import 'package:wordest/domain/state/home/home_state.dart';
import 'package:wordest/internal/dependencies/repository_module.dart';

class HomeModule {
  static HomeState homeState() {
    return HomeState(
      RepositoryModule.beerRepository(),
    );
  }
}