import 'package:dio/dio.dart';
import 'package:wordest/data/remote/service/beer_service.dart';
import 'package:wordest/data/repository/beer_data_repository.dart';
import 'package:wordest/domain/repository/beer_repository.dart';


class RepositoryModule {
  static BeerRepository? _beerRepository;

  static BeerRepository beerRepository() {
    if (_beerRepository == null) {
      _beerRepository = BeerDataRepository(
        BeerApi(Dio()),
      );
    }
    return _beerRepository!;
  }
}