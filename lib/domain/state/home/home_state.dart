import 'package:mobx/mobx.dart';

import 'package:wordest/domain/model/beer.dart';
import 'package:wordest/domain/repository/beer_repository.dart';

part 'home_state.g.dart';

class HomeState = HomeStateBase with _$HomeState;

abstract class HomeStateBase with Store {
  HomeStateBase(this._beerRepository);

  final BeerRepository _beerRepository;

  @observable
  Beer? beer;

  @observable
  bool isLoading = false;

  @action
  Future<void> getRandomBeer() async {
    isLoading = true;
    final data = await _beerRepository.getRandomBeer();
    beer = data;
    isLoading = false;
  }
}