// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_state.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeState on HomeStateBase, Store {
  final _$beerAtom = Atom(name: 'HomeStateBase.beer');

  @override
  Beer? get beer {
    _$beerAtom.reportRead();
    return super.beer;
  }

  @override
  set beer(Beer? value) {
    _$beerAtom.reportWrite(value, super.beer, () {
      super.beer = value;
    });
  }

  final _$isLoadingAtom = Atom(name: 'HomeStateBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$getRandomBeerAsyncAction = AsyncAction('HomeStateBase.getRandomBeer');

  @override
  Future<void> getRandomBeer() {
    return _$getRandomBeerAsyncAction.run(() => super.getRandomBeer());
  }

  @override
  String toString() {
    return '''
beer: ${beer},
isLoading: ${isLoading}
    ''';
  }
}
