
class Beer {
  final String name;
  final String tagline;
  final String firstBrewed;
  final String description;
  final String? imageUrl;
  final String abv;
  final String ibu;
  final String targetFg;
  final int? ebc;
  final double? ph;

  Beer({
    required this.name,
    required this.tagline,
    required this.firstBrewed,
    required this.description,
    required this.imageUrl,
    required this.abv,
    required this.ibu,
    required this.targetFg,
    required this.ebc,
    required this.ph,
  });
}
