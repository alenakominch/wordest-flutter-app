import 'package:wordest/domain/model/beer.dart';

abstract class BeerRepository {
  Future<Beer> getRandomBeer();
}